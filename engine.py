import csv
import os
import threading
import time
import traceback

import vk_api
from vk_api.execute import VkFunction

import config


class Parser:
    EXECUTE_FUNCTIONS_COUNT = config.execute_functions_count
    GROUP_IDS_COUNT = config.group_ids_count

    # :tokens: List
    # :ids_range: List/Tuple длиной 2 - диапазон id
    def __init__(self, tokens, ids_range):
        self.create_necessary_files()
        self.__vk_sessions: list = self.validate_correctness_of_tokens(tokens)
        self.__ids_generator = self.generate_list_of_ids_for_execute(*ids_range)    # Generator
        self.__bots = []
        self.__csv_file = open(config.path_to_csv, 'a')
        self.__csv_writer = csv.writer(self.__csv_file, delimiter=',')
        self.__lock_generator = threading.Lock()
        for vk_session in self.__vk_sessions:
            self.__bots.append(
                Bot(
                    vk_session, self.__ids_generator, self.__csv_writer, self.__lock_generator
                )
            )

    def create_necessary_files(self):
        if not os.path.exists(config.path_to_csv):
            with open(config.path_to_csv, 'w'):
                pass

    def validate_correctness_of_tokens(self, tokens) -> list:
        print('Проверка валидности токенов')
        vk_sessions = []
        for token in tokens:
            replace_token_str = '-' + token[-5:]
            session = vk_api.VkApi(token=token)
            session.short_name = replace_token_str
            try:
                session.method('groups.getById', {
                    'group_id': 1
                })
            except vk_api.exceptions.ApiError as e:
                if '[5]' in str(e):
                    print(f'Токен {replace_token_str} НЕвалидный')
            else:
                vk_sessions.append(session)
                print(f'Токен {replace_token_str} валидный')
        if not vk_sessions:
            raise NoValidTokens(
                'Метод "validate_correctness_of_tokens" не получил ни одного валидного токена')
        else:
            return vk_sessions

    def generate_list_of_ids_for_execute(self, start, finish):
        def chunk(s, n, is_str=False):
            chunk_list = []
            for k in range(0, len(s), n):
                if is_str:
                    chunk_list.append(','.join(
                        map(str, s[k:k + n])
                    ))
                else:
                    chunk_list.append(s[k:k + n])
            return chunk_list
        # При каждом вызове next() будет возвращать список "piece",
        # обработанный функцией "chunk"
        piece = []
        if start > finish:
            step = -1
            finish -= 1
        else:
            step = 1
            finish += 1
        for i in range(start, finish, step):
            piece.append(i)
            if len(piece) >= self.EXECUTE_FUNCTIONS_COUNT * self.GROUP_IDS_COUNT:
                yield chunk(piece, self.GROUP_IDS_COUNT, True)
                piece.clear()

    def run(self):
        try:
            for bot in self.__bots:
                bot.start()
                print(f'Бот {bot.short_name} запущен')
            for bot in self.__bots:
                bot.join()
        except Exception:
            print('Перехвачено (engine.py - run()')
            print(traceback.format_exc())
        finally:
            self.__csv_file.close()
            print(f'\nФайл "{config.path_to_csv}" закрыт')


class Bot(threading.Thread):
    def __init__(self, vk_session, ids_generator, csv_writer, lock_generator):
        super().__init__()
        self.__vk_session = vk_session
        self.__ids_generator = ids_generator
        self.__csv_writer = csv_writer
        self.__lock_generator = lock_generator
        self.short_name = self.name+'-'+self.__vk_session.short_name

    def run(self):
        vk_get_groups = VkFunction(
            args=('ids',),
            code='''
                var res = [];
                var ids = %(ids)s;
                var i = 0;

                while (i < ids.length) {
                    res.push(
                        API.groups.getById({
                            "group_ids": ids[i]
                            })
                    );
                    i = i + 1;
                }
                return res;
            '''
        )

        all_groups = []
        while True:
            try:
                self.__lock_generator.acquire()
                piece_of_ids = next(self.__ids_generator)
                self.__lock_generator.release()
                res = vk_get_groups(self.__vk_session, piece_of_ids)
                activated_group = 0
                for piece_of_groups in res:
                    for group in piece_of_groups:
                        if (
                                group.get('deactivated') is None and
                                group['is_closed'] == 0 and group['name'].strip()
                        ):
                            all_groups.append([group['id'], group['name']])
                            activated_group += 1
                percent_of_activated_group = int(
                    activated_group / config.parse_group_count_per_once * 100
                )
                for line in all_groups:
                    self.__csv_writer.writerow(line)
                all_groups.clear()
                first_id = int(piece_of_ids[0].split(',')[0])
                last_id = int(piece_of_ids[-1].split(',')[-1])
                first_id = f'{first_id:,}'.replace(',', '.')
                last_id = f'{last_id:,}'.replace(',', '.')

                print(
                    f'"{self.short_name}" - диапазон({first_id} - {last_id}) сохранен')
                if percent_of_activated_group < 1:
                    print(f'"{self.short_name}" - Групп больше нет')
                    break
            except StopIteration:
                self.__lock_generator.release()
                print(f'"{self.short_name}" - Список id закончился')
                break
            except KeyboardInterrupt:
                print(f'"{self.short_name}" - Завершено принудительно')
                break
            except ValueError as e:
                if 'generator already executing' in str(e):
                    print('Генератор занят')
                    continue
                else:
                    print(f'"{self.short_name}" - неизвестная ошибка, засыпание на 30 секунд')
                    print(traceback.format_exc())
                    time.sleep(30)
            except Exception:
                print(f'"{self.short_name}" - неизвестная ошибка, засыпание на 30 секунд')
                print(traceback.format_exc())
                time.sleep(30)

        if len(all_groups):
            print(len(all_groups))
            print(all_groups)


# Exceptions classes


class NoValidTokens(Exception):
    pass
