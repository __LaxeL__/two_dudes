# API ключ от сообщества two dudes
club_two_dudes_tokens = [
    '388e1e6f6a18266f3f8cb271f7b8e3077aec19d281acf843d170b820ffcfb6544ddd01daf0c8eb29f76de',
    '7940aa19e999762d038f5bf80cee7459281b0edf8247f9544c164b1175a998c61aa2cb36e7753e273e961',
    '625f9169fa96a62ba21c784d4af7e9c657d6a7863b49937bbced3d9fbc74172f26da47f0302c295463e47',
    'fefc059036c4c69c345052df7bd6795a53ca7521280eb30fe38de9b4576a776dd962f7206d7b2f7232060',
    '7c70daffce2be63ce2b085d76b9032d231d0f4961ffde8a8a260924cb56457c386769815088a2d4009f28',
]

# Перебор id от start_parse_id до finish_parse_id включая обе границы
start_parse_id = 1
finish_parse_id = 100_000
ids_count = finish_parse_id-start_parse_id+1

# Количество функций в execute и
# количество id групп в аргументе group_ids метода groups.getById
execute_functions_count = 25
group_ids_count = 500
parse_group_count_per_once = execute_functions_count * group_ids_count

path_to_csv = 'groups.csv'
