import time

import config
import engine


def main():
    start = time.time()
    ids_range = (config.start_parse_id, config.finish_parse_id)
    bot = engine.Parser(config.club_two_dudes_tokens, ids_range)
    bot.run()
    print(time.time() - start)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Завершено')
    except Exception as e:
        print('Неизвестная ошибка (main.py)')
        print(str(e))
